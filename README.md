# Gancio

Estibado de Gancio para Sindominio

## Instalación

Crear un fichero .env con los parámetros oportunos

```
 $ cp .env.sample .env
```

Edita las variables del fichero:

```
GANCIO_DATA_PATH="./data" # Volumen de los datos de gancio
GANCIO_PORT=13120 # Puerto
GANCIO_USER_GROUP="1000:1000" # Usuario de ejecución de gancio
SMTP_HOST="smtp.sindominio.net" # Servidor de correo SMTP
SMTP_PORT="587" # Puerto SMTP (25,587,465)
SMTP_SECURE="false" # TLS or not
SMTP_USER="gancio@sindominio.net" # Cuenta de correo para el envio saliente de mensajes de Gancio
SMTP_PASS="secret" # Password de la cuenta de correo
```

Crea la carpeta que hace de volumen con los permisos y propietarios:

```
$ mkdir data
$ sudo chown 1000:1000 data
```

Finalmente, levantamos el servicio:

```
$ docker-compose up -d
```

Y revisamos que el servicio está levantado correctamente:

```
$ docker ps 
```

Podemos mantener la ventana de log abierta para comprobar la actividad del servicio:

```
$ docker-compose logs -f
```

## Preconfiguración

Ahora que tu instancia está levantada en el puerto 13120 (en este caso de ejemplo), deberías configurar tu servidor de proxy inverso y el dominio.

En nuestro caso, configuramos primero que el dominio apunte a la IP pública de nuestro servidor (consulta la información de trastienda del uso de Knot)

Luego, creamos un fichero de configuración en nuestro servidor web que hace de proxy (en nuestro caso, Nginx) y configuramos los certificados ( en nuestro caso, Certbot).

Una plantilla que puedes usar para la configuración del servidor web, podría ser esta:

_/etc/nginx/sites-available/sub.dominio.gancio_

```
server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;
        server_name SUB.DOMINIO;

				client_max_body_size 20m;

				include snippets/ssl.conf;
        ssl_certificate /etc/letsencrypt/live/SUB.DOMINIO/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/SUB.DOMINIO/privkey.pem;

        error_log /var/log/nginx/SUB.DOMINIO.error.log;
        access_log off;

        location / {
                include snippets/proxy_pass_headers.conf; 
								proxy_pass http://docker.server:13120;
        }
}
```

Esto, en cada infraestructura puede ser diferente, utilizalo como referencia y adáptalo como mejor te convenga.

## Configuración

Al levantar el servicio por primera vez, un formulario de instalación te ofrecerá personalizar algunos datos.

**Importante:** Debes cambiar la localización de la base de datos al path que tenemos derecho de escritura.

![Cambiar el path de la base de datos](documentation/img/image000.png)

**Modifica** el path de la base de datos de _./gancio.sqlite_ a _/data/gancio.sqlite_

![Path Modificado](documentation/img/image001.png)

Ahora puedes pulsar _"Next"_ y acabar la instalación.

Configura correctamente los datos de la instancia y del correo electrónico que se va a utilizar para los envíos de mensajes a través de Gancio.

Más información de como administrar y trabajar con __Gancio__ en la web oficial [https://gancio.org/]()

# Links

Basado en:

* https://git.sindominio.net/gancio/docker-gancio
* https://framagit.org/les/gancio

# Contacto

Email: admin[at]sindominio.net
Matrix: #cafe:sindominio.net
